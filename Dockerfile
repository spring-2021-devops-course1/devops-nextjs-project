# Use node 14.16.1 as base image
FROM node:16.0
ENV LAST_UPDATED 2021-04-15

# Copy source code to /app directory in the container
COPY . /app

# Change working directory
WORKDIR /app

# Install dependencies and build
RUN npm install
RUN npm run build

# Expose API port to the outside
EXPOSE 3000

# Launch application
CMD ["npm","run", "start"]



