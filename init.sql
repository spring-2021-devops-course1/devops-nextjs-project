DROP TABLE IF EXISTS customers;

CREATE TABLE posts (
    id INTEGER PRIMARY KEY, title TEXT, content TEXT
);

INSERT INTO
    posts (id, title, content)
VALUES
    (1, 'First post - Bulwer-Lytton', 'It was a dark and stormy night...'),
    (2, 'Second post - Dickens', 'It was the best of times, it was the worst of times, it was the age of wisdom...');